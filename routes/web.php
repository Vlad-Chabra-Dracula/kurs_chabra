<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/guest/home');
});

Route::get(
    '/dentists','App\Http\Controllers\DentistController@allDentists')->name('dentists');

Route::get(
    '/order','App\Http\Controllers\DentistController@Dentists')->name('order');

Route::post(
    '/order/submit','App\Http\Controllers\PatientController@Order')->name('order-form');

Route::resource('/admin/patients', AdminController::class)->middleware('auth');

Auth::routes([
    'register' =>false,
    'verify' =>false,
    'reset' =>false
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
