<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Dentist extends Model
{
    use HasFactory;
    protected $table = 'dentists';
    public $timestamps = false;

    protected $primaryKey = 'dentist_id';

    public static function AllDentists() {
        return DB::table('dentists')->get();
    }
}
