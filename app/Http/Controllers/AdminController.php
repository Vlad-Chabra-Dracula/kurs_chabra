<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Dentist;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $patient = DB::table('patients as p')
            ->select(
                'p.patient_id',
                'p.name as patient',
                'p.date',
                'd.dentist_id',
                'd.name as dentist',
                'd.price')
            ->join(
                'dentists as d',
                'd.dentist_id',
                '=',
                'p.dentist_id')
            ->get();

        return view('admin.patients', ['data' => $patient]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function create()
    {
        return view('admin.create', ['dentists' => Dentist::AllDentists()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        $date = $request->input('date');
        $dentist_id = $request->input('dentist_id');

        $patient = new Patient();
        $patient->name = $name;
        $patient->date = $date;
        $patient->dentist_id = $dentist_id;
        $patient->save();
        return Redirect::to('/admin/patients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient = Patient::where('patient_id', $id)->first();

        return view('admin.edit', ['patient'=>$patient, 'data' => Dentist::AllDentists()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $patient = Patient::where('patient_id', $id)->first();

        $patient->name = $request->input('name');
        $patient->date = $request->input('date');
        $patient->dentist_id = $request->input('dentist_id');

        $patient->save();

        return redirect('admin/patients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Patient::destroy($id);
        return Redirect::to('/admin/patients');
    }
}
