<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function Order(Request $request) {
        $patient = new Patient();
        $patient ->name = $request->input('name');
        $patient ->date = $request->input('date');
        $patient ->dentist_id = $request->input('dentist_id');
        $patient ->save();
        return redirect()->route('dentists');
    }
}
