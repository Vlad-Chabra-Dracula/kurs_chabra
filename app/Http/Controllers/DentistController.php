<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Dentist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DentistController extends Controller
{
    public function allDentists() {
        $dentist = DB::table('dentists as d')
            ->select('d.name', 'd.price', 'p.patient_id', 'd.dentist_id')
            ->leftJoin('patients as p', 'd.dentist_id', '=', 'p.dentist_id')
            ->get();
        return view('guest.dentists', ['data' => $dentist]);
    }

    public function Dentists() {
        return view ('guest.order', ['data'=>Dentist::all()]);
    }
}
