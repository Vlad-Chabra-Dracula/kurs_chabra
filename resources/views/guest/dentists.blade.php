@extends('guest.layouts.layout')

@section('title')Всі лікарі@endsection

@section('content')

    <p>
    <div style="text-align: center;"><h1>Всі лікарі</h1></div>
    </p>

    <p>
    <table class="table table-hover table-bordered">

        <thead style="background-color: #212529; color: white">
        <tr>
            <th scope="col">Ім'я лікаря</th>
            <th scope="col">Ціна прийому</th>
            <th scope="col">Пацієнт</th>
        </tr>
        </thead>

        <tbody>
        @foreach($data as $element)
            <tr>
                <td>{{ $element->name}}</td>
                <td>{{ $element->price}}</td>
                <td>{{ $element->patient_id}}</td>
            </tr>
        @endforeach
        </tbody>

    </table>
    </p>


@endsection
