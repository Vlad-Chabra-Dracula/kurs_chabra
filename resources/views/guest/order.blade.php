@extends('guest.layouts.layout')

@section('title')Записатися на прийом @endsection

@section('content')

    <p>
    <h1>Записатися на прийом</h1>
    </p>

    <form action="{{ route('order-form') }}" method="post">

        @csrf

        <p>
            <label for="patient">Ваше прізвище:</label>
            <input type="text" name="name" id="patient" class="form-control" required>
        </p>

        <label for="date">Data</label>
        <input type="date" name="date" id="date" required>

        <p>
            <label for="dentist">Вибрати лікаря</label>
            <select name="dentist_id" id="dentist" class="form-control">

                @foreach($data as $dentist)

                    <option value="{{ $dentist->dentist_id }}">
                        {{ $dentist->name}}
                    </option>

                @endforeach

            </select>
        </p>

        <p>
            <button type="submit" class="btn btn-success">Підтвердити</button>
        </p>

    </form>

@endsection
