@extends('admin.layouts.layout')

@section('title')Редагувати інформацію про пацієнта@endsection

@section('content')

    <p>
    <h1>Редагувати інформацію про пацієнта</h1>
    </p>

    <form action="/admin/patients/{{ $patient->patient_id }}" method="post">
        @csrf
        @method('PUT')
        <p>
            <label>Ім'я пацієнта:</label>
            <input type="text" name="name" id="name" value="{{ $patient->name }}" required>
        </p>

        <p>
            <label>Data</label>
            <input type="date" name="date" id="date" value="{{ $patient->date }}" required>
        </p>
        <label for="dentist">Вибрати лікаря</label>
        <select name="dentist_id" id="dentist">
            @foreach($data as $dentist)

                <option value="{{ $dentist->dentist_id }}">
                    {{ $dentist->name}}
                </option>

            @endforeach

        </select>
        <input type="submit" value="Зберегти">
    </form>
@endsection
