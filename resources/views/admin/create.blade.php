@extends('admin.layouts.layout')

<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Додати пацієнта</h2>
    <form action="/admin/patients" method="POST">
        {{ csrf_field() }}
        <label for="patient">Прізвище пацієнта</label>
        <input type="text" name="name" id="patient" required>
        <p>
            <label>Дата</label>
            <input type="date" name="date" id="date" required>
        </p>
        <br/><br/>
        <label for="dentist">Вибрати лікаря</label>
        <select name="dentist_id" id="dentist">
            @foreach($dentists as $dentist)

                <option value="{{ $dentist->dentist_id }}">
                    {{ $dentist->name}}
                </option>

            @endforeach

        </select>

        <input type="submit" value="Зберегти">
    </form>
@endsection
