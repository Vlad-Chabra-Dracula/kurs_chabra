@extends('admin.layouts.layout')

@section('title')Пацієнти@endsection

@section('content')

    <p>
    <div style="text-align: center;"><h1>Пацієнти</h1></div>
    </p>

    <p>
    <table class="table table-hover table-bordered">

        <thead style="background-color: #212529; color: white">
        <tr>
            <th scope="col">Ім'я пацієнта</th>
            <th scope="col">Дата прийому</th>
            <th scope="col">Ціна прийому</th>
            <th scope="col">Ім'я лікаря</th>
        </tr>
        </thead>

        <tbody>
        @foreach($data as $element)
            <tr>
                <td>{{ $element->patient}}</td>
                <td>{{ $element->date}}</td>
                <td>{{ $element->price}}</td>
                <td>{{ $element->dentist}}</td>
                <td><a href="/admin/patients/{{ $element->patient_id}}/edit">
                        <button type="submit" class="btn btn-warning">Редагуємо</button>
                    </a></td>
                <td><form action="/admin/patients/{{ $element->patient_id}}" method="post">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger">Видаляємо</button>
                    </form></td>
            </tr>
        @endforeach
        </tbody>

    </table>
    </p>


@endsection
